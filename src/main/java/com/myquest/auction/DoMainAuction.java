package com.myquest.auction;

import com.myquest.auction.dao.DataUsersDAO;
import com.myquest.auction.dao.dataBase.DBUsersDAOMySql;
import com.myquest.auction.users.*;
import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.dao.DataProductsDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class DoMainAuction {

    /**Пользователь сервиса*/
    private User user;

    /**доступ к имеющимся продуктам*/
    @Autowired()
    @Qualifier("productsDAOMySql")
    private DataProductsDAO dataProductsDAO;

    /**доступ к имеющимся пользователям*/
    @Autowired()
    @Qualifier("usersDAOMySql")
    private DataUsersDAO dataUsersDAO;

    private static final Logger logger = LogManager.getLogger(DBUsersDAOMySql.class.getName());

    private String systemPassword =1234+"";

    public void authorization(String login, String password) {
        RegisteredUser user = dataUsersDAO.getUser(login);
        try {
            if (user.getPassword().equals(password)) {
                this.user = user;
                logger.info("Вход выполнен");
            }
        } catch (NullPointerException ignored) {
        }
    }

    public void register(RegisteredUser newUser) {
        if (dataUsersDAO.addUser(newUser))
            logger.info("Пользователь зарегистрирован");
    }

    public void EnterAsGuest() {
        user = new Guest();
        logger.info("Вход выполнен");
    }

    public List<Product> getProducts(Category category) {
        if (user != null)
            return dataProductsDAO.getProducts(category);
        logger.info("Вы не вошли");
        return null;
    }

    public List<Product> getProducts(String login) {
        if (this.user != null)
            return dataProductsDAO.getProducts(login);
        logger.info("Вы не вошли");
        return null;
    }

    public List<Product> getProductsNotRelevant(Category category) {
        if (accessСheck("Admin"))
                return dataProductsDAO.getProductsNotRelevant(category);
        return null;
    }

    public List<Product> getProductsNotRelevant(String login) {
        if (this.user.equals(user))
            return dataProductsDAO.getProductsNotRelevant(login);
        else
            if (accessСheck("Admin"))
                return dataProductsDAO.getProductsNotRelevant(login);
        return null;
}

    void addProduct(Product product) {
        if (accessСheck("RegisteredUser")|| accessСheck("Admin")) {
            dataProductsDAO.addProduct(product);
        }
    }

    RegisteredUser getUser(String login) {
        if (accessСheck("Admin"))
            return dataUsersDAO.getUser(login);
        return null;
    }

    RegisteredUser getUser(long id) {
        if (accessСheck("Admin"))
            return dataUsersDAO.getUser(id);
        return null;
    }

    public void addAdmin (String passwordSystem, String login, String password){
        if (passwordSystem.equals(systemPassword)){
            RegisteredUser admin = new Admin(login,password);
            dataUsersDAO.addUser(admin);
            logger.info("Создание Админа выполнено");
        } else {
            logger.info("Системны пароль не верный");
        }
    }

    public void changeSystemPassword (String oldPassword, String newPassword){
        if (systemPassword.equals(oldPassword)&& accessСheck("Admin")){
            systemPassword = newPassword;
        }
    }

    public void cancelGoods (long id) {
        Product product = dataProductsDAO.getProduct(id);
        if (product!=null){
            if (product.getUser().equals(user) || accessСheck("Admin")){
                product.disableProduct();
                logger.info("Товар отменен");
            }
        }else {
            logger.info("Такого товара не существует");
        }
    }

    public List<RegisteredUser> getUsersSortedByDateBirth () {
        if (accessСheck("Admin"))
            return dataUsersDAO.getUsersSortedByDateBirth();
        return null;
    }

    public List<RegisteredUser> getUsersSortedFirstName () {
        if (accessСheck("Admin"))
            return dataUsersDAO.getUsersSortedFirstName();
        return null;
    }

    private boolean accessСheck(String access) {
        try {
            if (user.getClass().getSimpleName().equals(access))
                return true;
            logger.info("Вы не " + access);
        } catch (NullPointerException e) {
            logger.info("Вы не вошли");
        }
        return false;
    }

    public User getUser() {
        return user;
    }
}
