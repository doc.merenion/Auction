package com.myquest.auction.users;


public class Admin extends RegisteredUser{

    private String position;


    public Admin(String login, String password) {
        super(login, password);
    }



    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    @Override
    public String toString() {
        return "Admin{" +
                "position='" + position + '\'' +
                ", login='" + login + '\'' +
                ", id=" + id +
                '}';
    }
}
