package com.myquest.auction.dao;

import com.myquest.auction.users.RegisteredUser;

import java.util.List;

public interface DataUsersDAO {
    RegisteredUser getUser (String login);
    RegisteredUser getUser (long id);
    boolean addUser (RegisteredUser user);
    boolean deleteUser (long id);
    List<RegisteredUser> getUsersSortedByDateBirth();
    List<RegisteredUser> getUsersSortedFirstName ();

}
