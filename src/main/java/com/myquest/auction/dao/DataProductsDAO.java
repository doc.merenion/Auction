package com.myquest.auction.dao;

import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.User;

import java.util.List;

public interface DataProductsDAO {

    List<Product> getAllProducts ();
    List<Product> getProducts (Category category);
    List<Product> getProducts (String login);
    Product getProduct (long id);
    List<Product> getProductsNotRelevant (Category category);
    List<Product> getProductsNotRelevant (String login);
    boolean addProduct (Product product);



}
