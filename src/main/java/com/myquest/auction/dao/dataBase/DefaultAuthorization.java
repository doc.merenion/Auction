package com.myquest.auction.dao.dataBase;

import com.myquest.auction.Main;

/**Класс содержащий параметры подключения*/
public class DefaultAuthorization {

    private static String url = "jdbc:mysql://localhost:3306/auction?useUnicode=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false";
    private static String login = "merenion";
    private static String pas = "772451";

    public static PullConnections getPullConnections() {
        try {
            return new PullCreatingСonnectionMySql(url,login,pas);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Main.log("Проблемы с драйвером");
        }
        return null;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        DefaultAuthorization.url = url;
    }

    public static String getLogin() {
        return login;
    }

    public static void setLogin(String login) {
        DefaultAuthorization.login = login;
    }

    public static String getPas() {
        return pas;
    }

    public static void setPas(String pas) {
        DefaultAuthorization.pas = pas;
    }
}
