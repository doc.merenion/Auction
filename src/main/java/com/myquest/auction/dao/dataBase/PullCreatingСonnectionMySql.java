package com.myquest.auction.dao.dataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Пулл без кеширования, заного создает подключения
 */
class PullCreatingСonnectionMySql implements  PullConnections{
    private String url, user, password;

    /**
     * создание пула, инициализация параметров подключения
     * @param url
     * @param user
     * @param password
     * @throws ClassNotFoundException проблемы с драйвером
     */
    PullCreatingСonnectionMySql(String url, String user, String password) throws ClassNotFoundException {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    @Override
    public Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public void putConnection(Connection connection) throws SQLException {
        connection.close();
    }
}