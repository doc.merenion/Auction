package com.myquest.auction.dao.dataBase;

import com.myquest.auction.dao.DataUsersDAO;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.Admin;
import com.myquest.auction.users.RegisteredUser;
import com.myquest.auction.users.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Component
@Lazy
@Qualifier("usersDAOMySql")
public class DBUsersDAOMySql implements DataUsersDAO {

    private final String TABLE_users =
            "CREATE TABLE IF NOT EXISTS users " +
                    "(id MEDIUMINT not null auto_increment, " +
                    "login CHAR(15) unique, " +
                    "password CHAR(30), " +
                    "firstName CHAR(25), " +
                    "secondName CHAR(25), " +
                    "lastName CHAR(25), " +
                    "dateBirth Date," +
                    "position CHAR(25)," +
                    "access CHAR(25)," +
                    " PRIMARY KEY (id))";

    private final String SELECT_userByLogin = "SELECT * from users where login = ?";
    private final String SELECT_userById = "SELECT * from users where id = ?";
    private final String INSERT_user = "INSERT INTO users (login,password,firstName,secondName,lastName," +
            "dateBirth,position,access) value (?,?,?,?,?,?,?,?)";
    private final String DELETE_user = "DELETE FROM users where id = ?";
    private final String ALL_USERS_bySorted = "SELECT * from users ORDER BY %s";

    private static final Logger logger = LogManager.getLogger(DBUsersDAOMySql.class.getName());
    private PullConnections pullConnections = DefaultAuthorization.getPullConnections();

    public DBUsersDAOMySql() {
        prepareTable();
    }

    @Override
    public RegisteredUser getUser(String login) {
        RegisteredUser registeredUser = null;
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            prSt = connection.prepareStatement(SELECT_userByLogin);
            prSt.setString(1, login);
            ResultSet rs = prSt.executeQuery();
            registeredUser = prepareUser(rs);
            logger.info("getUserByLogin - DONE");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (EmptyResultException e) {
            logger.error("getUserByLogin - Пользователь не найден");
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return registeredUser;
    }

    @Override
    public RegisteredUser getUser(long id) {
        RegisteredUser registeredUser = null;
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            prSt = connection.prepareStatement(SELECT_userById);
            prSt.setLong(1, id);
            ResultSet rs = prSt.executeQuery();
            registeredUser = prepareUser(rs);
            logger.info("getUserById - DONE");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (EmptyResultException e) {
            logger.error("getUserById - Пользователь не найден");
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return registeredUser;
    }

    @Override
    public boolean addUser(RegisteredUser user) {
        boolean result = true;
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            connection.setAutoCommit(false);
            prSt = connection.prepareStatement(INSERT_user);
            prSt.setString(1, user.getLogin());
            prSt.setString(2, user.getPassword());
            prSt.setString(3, user.getFirstName());
            prSt.setString(4, user.getSecondName());
            prSt.setString(5, user.getLastName());
            if (user.getDateBirth() != null) {
                prSt.setDate(6, new Date(user.getDateBirth().getTime()));
            } else
                prSt.setDate(6, null);
            prSt.setString(8, user.getClass().getSimpleName());
            if (user instanceof Admin) {
                prSt.setString(7, ((Admin) user).getPosition());
            } else {
                prSt.setString(7, null);
            }
            prSt.execute();
            connection.commit();
            logger.info("addUser - DONE");
        } catch (SQLIntegrityConstraintViolationException e) {
            result = false;
            logger.error(e.getMessage());
            logger.error("addUser - Пользователь с таким логином существует");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            result = false;
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return result;
    }

    @Override
    public boolean deleteUser(long id) {
        boolean result = true;
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            prSt = connection.prepareStatement(DELETE_user);
            prSt.setLong(1, id);
            prSt.execute();
            logger.info("deleteUser - DONE");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return false;
    }

    @Override
    public List<RegisteredUser> getUsersSortedByDateBirth() {
        List<RegisteredUser> users = getUsersByQuerySorting("dateBirth");
        logger.info("getUsersSortedByDateBirth - DONE");
        return users;
    }

    @Override
    public List<RegisteredUser> getUsersSortedFirstName() {
        List<RegisteredUser> users = getUsersByQuerySorting("firstName");
        logger.info("getUsersSortedFirstName - DONE");
        return users;
    }

    private void prepareTable() {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = pullConnections.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate(TABLE_users);
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (statement != null)
                    statement.close();
            } catch (SQLException ignored) {
            }
        }
    }

    /**
     * Сделал статическим чтобы использовать без создания экземпляра в другом классе.
     * Достает из ResultSet параметры пользователя и создает его экземпляр.
     * Считывает параметры с имеющийся строки.
     *
     * @param rs набор результатов
     * @return экземпляр пользователя
     * @throws SQLException         вдруг что..
     * @throws EmptyResultException если ResultSet пустой
     */
    public static RegisteredUser prepareUser(ResultSet rs) throws SQLException, EmptyResultException {
        if (rs.isBeforeFirst())
            rs.next();
        try {
            rs.getLong("id");
        } catch (SQLException e) {
            throw new EmptyResultException("prepareUser - Результат пустой");
        }
        RegisteredUser user = null;
        if (rs.getString("access").equals(Admin.class.getSimpleName())) {
            user = new Admin(rs.getString("login"), rs.getString("password"));
            ((Admin) user).setPosition(rs.getString("position"));
        } else if (rs.getString("access").equals(RegisteredUser.class.getSimpleName())) {
            user = new RegisteredUser(rs.getString("login"), rs.getString("password"));
        }
        if (user != null) {
            user.setId(rs.getLong("id"));
            user.setFirstName(rs.getString("firstName"));
            user.setSecondName(rs.getString("secondName"));
            user.setLastName(rs.getString("lastName"));
            try {
                user.setDateBirth(new java.util.Date(rs.getDate("dateBirth").getTime()));
            } catch (NullPointerException ignored) {
            }
        }
        return user;
    }

    /**
     * Выдает всех пользователей, отсортированных по заданному стобцу
     *
     * @param fieldForSorting наименнование столбца
     * @return отсортированные пользователи
     */
    private List<RegisteredUser> getUsersByQuerySorting(String fieldForSorting) {
        List<RegisteredUser> users = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = pullConnections.getConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format(ALL_USERS_bySorted, fieldForSorting));
            while (rs.next()) {
                users.add(prepareUser(rs));
            }
            logger.info("getUsersByQuerySorting - DONE");

        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (EmptyResultException e) {
            logger.error("getUsersByQuerySorting - Проблема с Результатом..");
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (statement != null)
                    statement.close();
            } catch (SQLException ignored) {
            }
        }
        return users;
    }
}
