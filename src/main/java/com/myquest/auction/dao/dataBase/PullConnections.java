package com.myquest.auction.dao.dataBase;

import java.sql.Connection;
import java.sql.SQLException;

public interface PullConnections {
    Connection getConnection() throws SQLException;
    void putConnection(Connection connection) throws SQLException;
}
