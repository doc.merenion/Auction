package com.myquest.auction.dao.dataBase;

import java.sql.SQLException;

/**
 * Используется в случае некоректного содержимого ResultSet
 */
public class EmptyResultException extends Exception {

    private String massage;

    public EmptyResultException(String massage) {
        this.massage = massage;
    }

    @Override
    public String toString() {
        return "EmptyResultException{" +
                "massage='" + massage + '\'' +
                '}';
    }
}
