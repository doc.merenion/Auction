package com.myquest.auction.dao.dataBase;

import com.myquest.auction.Main;
import com.myquest.auction.dao.DataProductsDAO;
import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Так как класс Product содержит ссылку на пользователя который его создал,
 * необходимо вытаскивать вместе с продуктом этого пользователя, думаю что проще было бы делать это используя класс
 * DBUsersDAOMySql, который уже имеет необходимую реализацию,
 * но этого не сделал - хотел поиграться с JOIN..
 */
@Component
@Lazy
@Qualifier("productsDAOMySql")
public class DBProductsDAOMySql implements DataProductsDAO {

    private final String TABLE_products = "CREATE TABLE if not exists products " +
            "(id MEDIUMINT not null auto_increment," +
            "name CHAR(40)," +
            "startingPrice FLOAT," +
            "currentRate FLOAT," +
            "startDate DATE," +
            "endDate DATE," +
            "category CHAR(25)," +
            "user_id int," +
            "cancellation boolean," +
            "PRIMARY KEY (id))";

    private final String ALL_PRODUCTS = "select * FROM products INNER JOIN users ON user_id = users.id WHERE cancellation = false";
    private final String SELECT_products = "select * FROM products INNER JOIN users ON user_id = users.id where %s = ? AND cancellation = ?";
    private final String INSERT_product = "insert into products (name,startingPrice,currentRate,startDate,endDate,category,user_id,cancellation) value (?,?,?,?,?,?,?,?)";

    private static final Logger logger = LogManager.getLogger(DBProductsDAOMySql.class.getName());
    private PullConnections pullConnections;

    public DBProductsDAOMySql() {
        pullConnections = DefaultAuthorization.getPullConnections();
        prepareTable();
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = pullConnections.getConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(ALL_PRODUCTS);
            while (rs.next()) {
                products.add(prepareProduct(rs));
            }
            logger.info("getAllProducts - DONE");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (EmptyResultException e) {
            logger.error("getAllProducts - Проблемы с результатом");
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (statement != null)
                    statement.close();
            } catch (SQLException ignored) {
            }
        }
        return products;
    }

    @Override
    public List<Product> getProducts(Category category) {
        List<Product> products = getProductsByQuery("category", category.toString(), false);
        if (products.size() > 0)
            logger.info("getProductsByCategory - DONE");
        return products;
    }

    @Override
    public List<Product> getProducts(String login) {
        List<Product> products = getProductsByQuery("users.login", login, false);
        if (products.size() > 0)
            logger.info("getProductsByLogin - DONE");
        return products;
    }

    @Override
    public Product getProduct(long id) {
        List<Product> products = getProductsByQuery("products.id", String.valueOf(id), true);
        if (products.size() == 0)
            products = getProductsByQuery("products.id", String.valueOf(id), false);
        if (products.size() != 0) {
            logger.info("getProductByID - DONE");
            return products.get(0);
        }
        return null;
    }

    @Override
    public List<Product> getProductsNotRelevant(Category category) {
        List<Product> products = getProductsByQuery("category", category.toString(), true);
        logger.info("getProductsNotRelevant By Category - DONE");
        return products;
    }

    @Override
    public List<Product> getProductsNotRelevant(String login) {
        List<Product> products = getProductsByQuery("users.login", login, true);
        logger.info("getProductsNotRelevant By login - DONE");
        return products;
    }

    @Override
    public boolean addProduct(Product product) {
        boolean result = true;
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            connection.setAutoCommit(false);
            prSt = connection.prepareStatement(INSERT_product);
            prSt.setString(1, product.getName());
            prSt.setFloat(2, product.getStartingPrice());
            prSt.setFloat(3, product.getCurrentRate());
            prSt.setDate(4, new java.sql.Date(product.getStartDate().getTime()));
            prSt.setDate(5, new java.sql.Date(product.getEndDate().getTime()));
            prSt.setString(6, product.getCategory().toString());
            prSt.setLong(7, product.getUser().getId());
            prSt.setBoolean(8, product.isCancellation());
            prSt.execute();
            connection.commit();
            logger.info("addProduct - DONE");
        } catch (SQLIntegrityConstraintViolationException e) {
            result = false;
            e.printStackTrace();
            logger.error("addProduct - Такой продукт уже существует");
        } catch (SQLException e) {
            result = false;
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return result;
    }

    /**
     * достает продукты по соответствию заданного значения
     *
     * @param field                название столбца
     * @param value                значение ячейки столбца по которому вытаскиваются продукты
     * @param cancellationProducts искать среди актуальных или нет
     * @return продукты подходящие по условию field = value
     */
    private List<Product> getProductsByQuery(final String field, final String value, boolean cancellationProducts) {
        List<Product> products = new ArrayList<>();
        Connection connection = null;
        PreparedStatement prSt = null;
        try {
            connection = pullConnections.getConnection();
            prSt = connection.prepareStatement(String.format(SELECT_products, field));
            prSt.setString(1, value);
            prSt.setBoolean(2, cancellationProducts);
            ResultSet rs = prSt.executeQuery();
            while (rs.next()) {
                products.add(prepareProduct(rs));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (EmptyResultException e) {
            logger.error("getProductsByQuery - Пользователь не найден");
        } finally {
            try {
                if (connection != null)
                    pullConnections.putConnection(connection);
                if (prSt != null)
                    prSt.close();
            } catch (SQLException ignored) {
            }
        }
        return products;
    }

    private void prepareTable() {
        try {
            Connection connection = pullConnections.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(TABLE_products);
            statement.close();
            pullConnections.putConnection(connection);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private Product prepareProduct(ResultSet rs) throws SQLException, EmptyResultException {
        try {
            rs.getLong("id");
        } catch (SQLException e) {
            throw new EmptyResultException("Результат пустой");
        }
        Product product = new Product(rs.getString("name"),
                rs.getFloat("startingPrice"),
                new Date(rs.getDate("endDate").getTime()),
                Category.valueOf(rs.getString("category"))
                , DBUsersDAOMySql.prepareUser(rs));
        product.setStartDate(new Date(rs.getDate("startDate").getTime()));
        product.setId(rs.getLong("id"));
        product.setCurrentRate(rs.getFloat("currentRate"));
        product.setCancellation(rs.getBoolean("cancellation"));
        return product;
    }
}
