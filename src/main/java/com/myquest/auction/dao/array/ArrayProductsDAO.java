package com.myquest.auction.dao.array;

import com.myquest.auction.Main;
import com.myquest.auction.dao.DataProductsDAO;
import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Lazy
public class ArrayProductsDAO implements DataProductsDAO {

    private List<Product> products = new ArrayList<>();

    @Override
    public List<Product> getAllProducts() {
        return products;
    }

    @Override
    public List<Product> getProducts(Category category) {
        return products.stream().filter(x -> x.getCategory().equals(category)).collect(Collectors.toList());
    }

    @Override
    public List<Product> getProducts(String login) {
        return products.stream().filter(x -> x.getUser().getLogin().equals(login)).collect(Collectors.toList());
    }

    @Override
    public Product getProduct(long id) {
        return products.stream().filter(x -> x.getId().equals(id)).findAny().get();
    }

    @Override
    public List<Product> getProductsNotRelevant(Category category) {
        return products.stream().filter(x -> x.getEndDate().getTime()<(new Date()).getTime() || x.isCancellation()).collect(Collectors.toList());
    }

    @Override
    public List<Product> getProductsNotRelevant(String login) {
        return products.stream().filter(x -> (x.getEndDate().getTime()<(new Date()).getTime() || x.isCancellation()) && x.getUser().getLogin().equals(login)) .collect(Collectors.toList());
    }

    @Override
    public boolean addProduct(Product product) {
        boolean result = true;
        try {
            if (!products.contains(product)) {
                products.add(product);
                Main.log("Лот добавлен в базу");
            } else
                Main.log("Такой товар уже существует");
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }
        return result;
    }
}
