package com.myquest.auction.dao.array;

import com.myquest.auction.Main;
import com.myquest.auction.dao.DataUsersDAO;
import com.myquest.auction.users.RegisteredUser;
import com.myquest.auction.users.User;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Lazy
public class ArrayUsersDAO implements DataUsersDAO {

    private List<RegisteredUser> users = new ArrayList<>();

    @Override
    public RegisteredUser getUser(String login) {
        Optional<RegisteredUser> userO = users.stream().filter(x -> x.getLogin().equals(login)).findAny();
        if (userO.isPresent())
            return userO.get();
        else
            Main.log("Пользователь не найден");
        return null;
    }

    @Override
    public RegisteredUser getUser(long id) {
        Optional<RegisteredUser> userO = users.stream().filter(x -> x.getId() == id).findAny();
        if (userO.isPresent())
            return userO.get();
        else
            Main.log("Пользователь не найден");
        return null;
    }

    @Override
    public boolean addUser(RegisteredUser user) {
        boolean result = true;
        try {
            if (users.stream().noneMatch(x -> x.getLogin().equals(user.getLogin()))) {
                users.add(user);
                Main.log("Пользователь добавлен в базу");
            } else
                Main.log("Пользователь с таким логином существует");
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteUser(long id) {
        User user = getUser(id);
        if (user != null)
        users.remove(user);
        return false;
    }

    /**
     * Сделаю позже если необходимо
     * @return
     */
    @Override
    public List<RegisteredUser> getUsersSortedByDateBirth() {
        return null;
    }

    /**
     * Сделаю позже если необходимо
     * @return
     */
    @Override
    public List<RegisteredUser> getUsersSortedFirstName() {
        return null;
    }
}
