package com.myquest.auction;


import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.Admin;
import com.myquest.auction.users.RegisteredUser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.logging.Logger;

@ComponentScan(basePackages = "com.reactionsSupport.beam")
public class Main {

    private static ApplicationContext context = new ClassPathXmlApplicationContext("AnnotationsOn_Context.xml");

    public static void main(String[] args) {
        ConsoleUI consoleUI = (ConsoleUI) context.getBean("consoleUI");
        log("Приветствуем на нашем аукционе. Введите help для помощи");
        consoleUI.startInterface();
    }

    public static void log (String massage){
        System.err.println(massage);
    }
}
