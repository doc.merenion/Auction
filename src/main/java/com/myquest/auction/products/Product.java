package com.myquest.auction.products;

import com.myquest.auction.dao.dataBase.DBUsersDAOMySql;
import com.myquest.auction.users.RegisteredUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;

public class Product {

    /**
     * id В БД переопределяется
     */
    private static long createdId;
    {
        createdId++;
    }

    private long id = createdId;
    private String name;
    private float startingPrice;
    private float currentRate;
    private Date startDate;
    private Date endDate;
    private Category category;
    private RegisteredUser user;

    private static final Logger logger = LogManager.getLogger(Product.class.getName());

    public void setCancellation(boolean cancellation) {
        this.cancellation = cancellation;
    }

    private boolean cancellation = false;

    {
        this.startDate = new Date();
    }

    public Product(String name, float startingPrice,Date endDate, Category category, RegisteredUser user) {
        this.name = name;
        this.startingPrice = startingPrice;
        this.endDate = endDate;
        this.category = category;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(float startingPrice) {
        this.startingPrice = startingPrice;
    }

    public float getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(float currentRate) {
        this.currentRate = currentRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public RegisteredUser getUser() {
        return user;
    }

    public void setUser(RegisteredUser user) {
        this.user = user;
    }

    public boolean isCancellation() {
        return cancellation;
    }

    public void disableProduct() {
        this.cancellation = true;
    }

    public void placeABet(float bet) {
        if (bet>startingPrice && bet>this.currentRate)
            this.currentRate = bet;
        else
            logger.error("setCurrentRate - некорректная ставка");
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return
                Float.compare(product.startingPrice, startingPrice) == 0 &&
                Float.compare(product.currentRate, currentRate) == 0 &&
                cancellation == product.cancellation &&
                Objects.equals(name, product.name) &&
                category == product.category &&
                Objects.equals(user, product.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, startingPrice, currentRate, startDate, endDate, category, user, cancellation);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startingPrice=" + startingPrice +
                ", currentRate=" + currentRate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", category=" + category +
                ", user=" + user +
                ", cancellation=" + cancellation +
                '}';
    }
}
