package com.myquest.auction;

import com.myquest.auction.dao.dataBase.DBUsersDAOMySql;
import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.RegisteredUser;
import com.myquest.auction.users.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

@Component
public class ConsoleUI {

    @Autowired
    private DoMainAuction auction;
    private final String ANSI_YELLOW = "\u001B[33m";
    private final String ANSI_RESET = "\u001B[0m";

    private static final Logger logger = LogManager.getLogger(ConsoleUI.class.getName());


    public synchronized void startInterface() {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] arrayLine = line.split("\"");
        try {
            switch (arrayLine[0]) {
                case "reg ":
                    RegisteredUser user = new RegisteredUser(arrayLine[1], arrayLine[2]);
                    auction.register(user);
                    break;
                case "sign in ":
                    auction.authorization(arrayLine[1], arrayLine[2]);
                    break;
                case "log in guest":
                    auction.EnterAsGuest();
                    break;
                case "get p from category ":
                    List<Product> products = null;
                    products = auction.getProducts(Category.valueOf(arrayLine[1]));
                    if (products.size()==0){
                        logger.info("пусто");
                        break;
                    }
                    for (Product product : products)
                        logger.info("\n"+products.indexOf(product) + ") " + product);
                    break;
                case "all category":
                    for (Category category : Category.values())
                        logger.info("\n"+category.toString());
                    break;
                case "get p from user ":
                    List<Product> products1 = auction.getProducts(arrayLine[1]);
                    if (products1.size()==0){
                        logger.info("пусто");
                        break;
                    }
                    for (Product product : products1)
                        logger.info("\n"+products1.indexOf(product) + ") " + product);
                    break;
                case "get p not relevant from category ":
                    List<Product> products2 = null;
                    products2 = auction.getProductsNotRelevant(Category.valueOf(arrayLine[1]));
                    if (products2.size()==0){
                        logger.info("пусто");
                        break;
                    }
                    for (Product product : products2)
                        logger.info("\n"+products2.indexOf(product) + ") " + product);
                    break;
                case "get p not relevant from user ":
                    List<Product> products3 = null;
                    products3 = auction.getProductsNotRelevant(arrayLine[1]);
                    if (products3.size()==0){
                        logger.info("пусто");
                        break;
                    }
                    for (Product product : products3)
                        logger.info("\n"+products3.indexOf(product) + ") " + product);
                    break;
                case "add product ":
                    Date endDate = null;
                    try{
                        String stringDate=arrayLine[3] ;
                        endDate = new SimpleDateFormat("dd/MM/yyyy").parse(stringDate);
                    }catch(Exception e){
                        logger.info("Дата указана неверно");
                        break;
                    }
                    Product product = new Product(arrayLine[1],Float.valueOf(arrayLine[2]),endDate,Category.valueOf(arrayLine[4]),(RegisteredUser) auction.getUser());
                    auction.addProduct(product);
                    break;
                case "my profile":
                    logger.info(auction.getUser().toString());
                    break;
                case "add admin ":
                    auction.addAdmin(arrayLine[1],arrayLine[2],arrayLine[3]);
                    break;
                case "users sorted by date birth":
                    for (RegisteredUser user1 : auction.getUsersSortedByDateBirth())
                        logger.info("\n"+ user1);
                    break;
                case "users sorted by first name":
                    for (RegisteredUser user2 : auction.getUsersSortedFirstName())
                        logger.info("\n"+ user2);
                    break;
                case "my products":
                        List<Product> products4 = auction.getProducts(((RegisteredUser) auction.getUser()).getLogin());
                    if (products4.size()==0){
                        logger.info("пусто");
                        break;
                    }
                    for (Product product2 : products4)
                        logger.info("\n"+products4.indexOf(product2) + ") " + product2);
                    break;
                case "get info user ":
                    User user1 = auction.getUser(arrayLine[1]);
                    if (user1!= null)
                        logger.info(user1.toString());
                    break;
                case "cancel goods ":
                    auction.cancelGoods(Long.parseLong(arrayLine[1]));
                    break;
                case "help":
                    logger.info("\n"+ANSI_YELLOW + getHelpFromFile(new File("src/main/resources/help.txt")) + ANSI_RESET);
                    break;
                case "change system password ":
                    auction.changeSystemPassword(arrayLine[1],arrayLine[2]);
                    break;
                default:
                    logger.info(ANSI_YELLOW + "Неверная команда!" + ANSI_RESET);
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            logger.info(ANSI_YELLOW + "Неверная команда!Возврат в главное меню." + ANSI_RESET);
            startInterface();
        } catch (IllegalArgumentException e) {
            logger.info("Категорию необходимо вводить большими буквами, например HOME.");
        } catch (NullPointerException ignored){}
        startInterface();
    }

    private String getHelpFromFile(File file) {
        StringBuilder text = new StringBuilder("");
        try {
            Scanner scanner = new Scanner(file);
            do {
                text.append(scanner.nextLine()).append("\n");
            } while (scanner.hasNext());
        } catch (FileNotFoundException e) {
            logger.info("Файл с help информацией не найден.");
            //e.printStackTrace();
        }
        return text.toString();
    }
}
