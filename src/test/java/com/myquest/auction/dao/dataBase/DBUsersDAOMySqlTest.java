package com.myquest.auction.dao.dataBase;

import com.myquest.auction.dao.DataUsersDAO;
import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.Admin;
import com.myquest.auction.users.RegisteredUser;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Делал только для себя.. Работают только с пустыми таблицами
 */
public class DBUsersDAOMySqlTest {

    @Test
    public void getUsersSortedByDateBirth() {
        DataUsersDAO db = new DBUsersDAOMySql();

        Date date1 = new Date(2012,10,9);
        Date date2 = new Date(2011,10,9);
        Date date3 = new Date(2008,10,9);
        Date date4 = new Date(2013,10,9);
        Date date5 = new Date(2014,10,9);

        RegisteredUser user1 = new RegisteredUser("user1","1234");
        RegisteredUser user2 = new RegisteredUser("user2","1234");
        RegisteredUser user3 = new RegisteredUser("user3","1234");
        RegisteredUser user4 = new RegisteredUser("user4","1234");
        RegisteredUser user5 = new RegisteredUser("user5","1234");

        user1.setDateBirth(date1);
        user2.setDateBirth(date2);
        user3.setDateBirth(date3);
        user4.setDateBirth(date4);
        user5.setDateBirth(date5);

        db.addUser(user1);
        db.addUser(user2);
        db.addUser(user3);
        db.addUser(user4);
        db.addUser(user5);

        List<RegisteredUser> users = db.getUsersSortedByDateBirth();

        db.deleteUser(users.get(0).getId());
        db.deleteUser(users.get(1).getId());
        db.deleteUser(users.get(2).getId());
        db.deleteUser(users.get(3).getId());
        db.deleteUser(users.get(4).getId());

        assertEquals(users.get(0), user3);
        assertEquals(users.get(1), user2);
        assertEquals(users.get(2), user1);
        assertEquals(users.get(3), user4);
    }

    @Test
    public void getUsersSortedFirstName() {
        DataUsersDAO db = new DBUsersDAOMySql();

        RegisteredUser user1 = new RegisteredUser("user1","1234");
        RegisteredUser user2 = new RegisteredUser("user2","1234");
        RegisteredUser user3 = new RegisteredUser("user3","1234");
        RegisteredUser user4 = new RegisteredUser("user4","1234");

        user1.setFirstName("d1ffgg");
        user2.setFirstName("a9ffgg");
        user3.setFirstName("cffgg");
        user4.setFirstName("bffgg");

        db.addUser(user1);
        db.addUser(user2);
        db.addUser(user3);
        db.addUser(user4);

        List<RegisteredUser> users = db.getUsersSortedFirstName();

        db.deleteUser(users.get(0).getId());
        db.deleteUser(users.get(1).getId());
        db.deleteUser(users.get(2).getId());
        db.deleteUser(users.get(3).getId());

        assertEquals(users.get(0), user2);
        assertEquals(users.get(1), user4);
        assertEquals(users.get(2), user3);
        assertEquals(users.get(3), user1);
    }

    @Test
    public void testingAll() {
        Date dateNow = new Date();

        RegisteredUser user1 = new RegisteredUser("user1","1234");
        RegisteredUser user2 = new RegisteredUser("user2","12345");
        Admin admin = new Admin("user_admin","123456");
        user2.setDateBirth(dateNow);
        user2.setFirstName("FirstName");
        user2.setLastName("LastName");
        user2.setSecondName("SecondName");
        admin.setPosition("super admin");
        DataUsersDAO db = new DBUsersDAOMySql();
        db.addUser(user1);
        db.addUser(user2);
        db.addUser(admin);
        db.addUser(admin);
        RegisteredUser user1ByLogin = db.getUser("user1");
        RegisteredUser user1ById = db.getUser(user1ByLogin.getId());
        RegisteredUser user2ByLogin= db.getUser("user2");
        RegisteredUser user2ById= db.getUser(user2ByLogin.getId());
        RegisteredUser adminByLogin = db.getUser("user_admin");
        RegisteredUser adminByLoginde = db.getUser("usdeer_admin"); //не существующий
        RegisteredUser adminById = db.getUser(adminByLogin.getId());
        db.deleteUser(user1ByLogin.getId());
        db.deleteUser(user2ByLogin.getId());
        db.deleteUser(adminByLogin.getId());

        assertEquals("user1", user1ByLogin.getLogin());
        assertEquals("1234", user1ByLogin.getPassword());

        assertEquals("user1", user1ById.getLogin());
        assertEquals("1234", user1ById.getPassword());

        assertEquals("user2", user2ByLogin.getLogin());
        assertEquals("12345", user2ByLogin.getPassword());
        assertEquals("FirstName", user2ByLogin.getFirstName());
        assertEquals("SecondName", user2ByLogin.getSecondName());
        assertEquals("LastName", user2ByLogin.getLastName());
//        assertEquals(new java.sql.Date(dateNow.getTime()), new java.sql.Date(user2ById.getDateBirth().getTime()));

        assertEquals("user2", user2ById.getLogin());
        assertEquals("12345", user2ById.getPassword());
        assertEquals("FirstName", user2ById.getFirstName());
        assertEquals("SecondName", user2ById.getSecondName());
        assertEquals("LastName", user2ById.getLastName());
//        assertEquals(new java.sql.Date(dateNow.getTime()), new java.sql.Date(user2ByLogin.getDateBirth().getTime()));

        assertEquals("user_admin", adminByLogin.getLogin());
        assertEquals("123456", adminByLogin.getPassword());
        assertEquals("super admin", ((Admin)adminByLogin).getPosition());

        assertEquals("user_admin", adminById.getLogin());
        assertEquals("123456", adminById.getPassword());
        assertEquals("super admin", ((Admin)adminById).getPosition());
    }
}