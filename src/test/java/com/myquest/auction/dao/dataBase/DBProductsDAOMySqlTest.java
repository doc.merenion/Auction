package com.myquest.auction.dao.dataBase;

import com.myquest.auction.products.Category;
import com.myquest.auction.products.Product;
import com.myquest.auction.users.RegisteredUser;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class DBProductsDAOMySqlTest {

    /**
     * Делать нормальный тест времени небыло.. По этому он работает только когда таблица продуктов пуста
     * Сделаю все как надо при необходимости. А пока только для себя..
     */
    @Test
    public void allTest() {
        DBUsersDAOMySql usersDAOMySql = new DBUsersDAOMySql();
        DBProductsDAOMySql productsDAOMySql = new DBProductsDAOMySql();

        RegisteredUser newUser = new RegisteredUser("Lol","D123");
        usersDAOMySql.addUser(newUser);

        RegisteredUser user = usersDAOMySql.getUser("Lol");

        Product product1 = new Product("Скунский камень",20_000.5f,new Date(2020,8,23), Category.RELIC,user);
        Product product2 = new Product("Коготь Бездомного пса",430_000f,new Date(2025,4,10),Category.RELIC,user);
        Product product3 = new Product("Зуб Будды",133_000,new Date(2022,7,12),Category.HOME,user);
        product3.setCurrentRate(200000);
        Product product4 = new Product("Голова Святой Екатерины",400_000f,new Date(2023,4,10),Category.AUTO,user);
        product4.setCurrentRate(1_000_000);
        product4.setCancellation(true);
        Product product5 = new Product("Саркофаг моих воспоминаний",450_000f,new Date(2043,11,13),Category.AUTO,user);
        product5.disableProduct();

        productsDAOMySql.addProduct(product1);
        productsDAOMySql.addProduct(product2);
        productsDAOMySql.addProduct(product3);
        productsDAOMySql.addProduct(product4);
        productsDAOMySql.addProduct(product5);

        List<Product> allProducts = productsDAOMySql.getAllProducts();
        List<Product> productsByLogin = productsDAOMySql.getProducts("Lol");
        List<Product> productsByLoginNotExist = productsDAOMySql.getProducts("LolNON"); //не существующий
        List<Product> productsByCategory = productsDAOMySql.getProducts(Category.RELIC);
        List<Product> productsByCategoryNotExist = productsDAOMySql.getProducts(Category.ELECTRONICS);
        List<Product> productsNotRelevantByLogin = productsDAOMySql.getProductsNotRelevant("Lol");
        List<Product> productsNotRelevantByLoginNotExist = productsDAOMySql.getProductsNotRelevant("LolNON");//не существующий
        List<Product> productsNotRelevantByCategory = productsDAOMySql.getProductsNotRelevant(Category.AUTO);
        List<Product> productsNotRelevantByCategoryNotExist = productsDAOMySql.getProductsNotRelevant(Category.ELECTRONICS);
        Product productById = productsDAOMySql.getProduct(productsDAOMySql.getProducts(Category.HOME).get(0).getId());

        assertEquals(3, allProducts.size());
        assertEquals(3, productsByLogin.size());
        assertEquals(0, productsByLoginNotExist.size());
        assertEquals(2, productsByCategory.size());
        assertEquals(0, productsByCategoryNotExist.size());
        assertEquals(2, productsNotRelevantByLogin.size());
        assertEquals(0, productsNotRelevantByLoginNotExist.size());
        assertEquals(2, productsNotRelevantByCategory.size());
        assertEquals(0, productsNotRelevantByCategoryNotExist.size());

        assertEquals(productById, product3);

        assertTrue(allProducts.contains(product1));
        assertTrue(allProducts.contains(product2));
        assertTrue(allProducts.contains(product3));

        assertTrue(productsByLogin.contains(product1));
        assertTrue(productsByLogin.contains(product2));
        assertTrue(productsByLogin.contains(product3));

        assertTrue(productsByCategory.contains(product1));
        assertTrue(productsByCategory.contains(product2));

        assertTrue(productsNotRelevantByLogin.contains(product4));
        assertTrue(productsNotRelevantByLogin.contains(product5));

        assertTrue(productsNotRelevantByCategory.contains(product4));
        assertTrue(productsNotRelevantByCategory.contains(product5));
    }
}